<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCardapioAddForeignBar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('cardapio', function(Blueprint $table) {
           $table->integer('bar_id')->unsigned();
           $table->foreign('bar_id')
                   ->references('id')
                   ->on('bar');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cardapio', function(Blueprint $table) {
           $table->dropColumn('bar_id');
           
        });
    }
}
