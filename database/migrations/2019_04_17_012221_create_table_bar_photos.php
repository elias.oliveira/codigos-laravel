<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBarPhotos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bar_photos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('bar_id');
            $table->string('photo');
            $table->timestamps();
            
            $table->foreing('bar_id')->references('id')->on('bar');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bar_photos');
    }
}
