<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/bar/{slug}', 'HomeController@get')->name('home.single');

Route::group(['middleware' => ['auth'] ], function(){
    
    Route::prefix('admin')->namespace('Admin')->group(function(){ 
        Route::prefix('bares')->group(function(){
            Route::get('',  'barController@index')->name('bar.home');
            Route::get('new',  'barController@novo')->name('bar.new');
            Route::post('store',  'barController@store')->name('bar.store');
            Route::get('edit/{bar}',  'barController@edit')->name('bar.edit');
            Route::post('update/{id}',  'barController@update')->name('bar.update');
            Route::get('remove/{id}',  'barController@delete')->name('bar.remove');
            
            Route::get('/photos/{id}',  'BarPhotoController@index')->name('bar.photo');
            Route::post('/photos/{id}',  'BarPhotoController@save')->name('bar.photo.save');
        });
        
        Route::prefix('users')->group(function(){
            Route::get('',  'UserController@index')->name('user.home');
            Route::get('new',  'UserController@novo')->name('user.new');
            Route::post('store',  'UserController@store')->name('user.store');
            Route::get('edit/{user}',  'UserController@edit')->name('user.edit');
            Route::post('update/{id}',  'UserController@update')->name('user.update');
            Route::get('remove/{id}',  'UserController@delete')->name('user.remove');
        });
        
         Route::prefix('cardapios')->group(function(){
            Route::get('',  'CardapioController@index')->name('cardapio.home');
            Route::get('new',  'CardapioController@novo')->name('cardapio.new');
            Route::post('store',  'CardapioController@store')->name('cardapio.store');
            Route::get('edit/{cardapio}',  'CardapioController@edit')->name('cardapio.edit');
            Route::post('update/{id}',  'CardapioController@update')->name('cardapio.update');
            Route::get('remove/{id}',  'CardapioController@delete')->name('cardapio.remove');
        });

    });
});

Route::get('rel', function(){
   $bar = \App\Bar::find(1); 
   print $bar->nome;
   
   print'<br>';
   foreach( $bar->cardapios as $cardapio ){
       print'Item cardápio: ' . $cardapio->nome .' Preço: '. $cardapio->preco .  '<br>';
   }
   
   dd($bar->cardapios);
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
