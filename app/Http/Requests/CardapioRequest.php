<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CardapioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|min:5',
            'preco' => 'required',
            'bar_id' => 'required'
        ];
    }
    
    public function messages()
    {
        return [
          'nome.required' => "Campo Nome é obrigatório!",
          'preco.required' => "Campo Preço é obrigatório!",
          'bar_id.required' => "Campo Bar é obrigatório!",
          '*.min' => 'Quantidade mínima de 5 caracteres'  
        ];
    }
}
