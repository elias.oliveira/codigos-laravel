<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //
    public function index(){
        // Assim trago todos 
        $users = User::all();
        // Assim friltro apneas o meu usuario
        $users = User::where('id', Auth::user()->id)->get();
        return view('admin.users.index', compact('users'));
    }
    
    public function novo(){
        return view('admin.users.store');
    }
    public function store(UserRequest $request){
        $UserData = $request->all();
        
        $request->validated();
        
        $UserData['password'] = bcrypt($UserData['password']);
        
        $user = new User();
        $user->create($UserData);
        
        flash('Usuário criado com sucesso')->success();
        return redirect()->route('user.home');
    }
    public function edit(User $user){ // aqui automaticamente pelo id ele ja traz o objeto
        
        return view('admin.users.edit', compact('user'));
        
    }
    
    public function update(UserRequest $request, $id){
        $UserData = $request->all();
        
        $validator = $request->validated();
        
        $user = User::findOrFail($id); // aqui eu busco as informacoes do objeto pelo id, no caso o que identica e o mesmo nome da rota
        
        $user->update($UserData);
        
        flash('Usuário atualizado com sucesso')->success();
        return redirect()->route('user.edit', ['user' => $id]);
    }
    
    public function delete($id){
        
        
        $user = User::findOrFail($id); // aqui eu busco as informacoes do objeto pelo id, no caso o que identica e o mesmo nome da rota
        $user->delete();
        
        flash('Usuário removido com sucesso')->success();
        return redirect()->route('user.home');
    }
    
    
}
