<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bar;

class BarPhotoController extends Controller
{
    //
    public function index($id){

        $bar_id = $id;
        return view('admin.bares.photos.index', compact('bar_id'));
    }
    
    public function save(Request $request, $id){
        foreach($request->file('photos') as $photo){
            
            $newName = sha1($photo->getClientOriginalName()) . uniqid() . '.' . $photo->getClientOriginalExtension();
            $photo->move(public_path('images'), $newName);
            $bar = new Bar;
            $bar = $bar->find($id);
            
            $bar->photos()->create([
                'photo' => $newName
            ]);
        }
        flash()->success('Upload de Fotos Realizado com Sucesso!');
        return redirect()->route('bar.photo', ['id'=> $id]);
    }
}
