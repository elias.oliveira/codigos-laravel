<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CardapioRequest;
use App\Cardapio;
use App\Bar;
use Auth;

class CardapioController extends Controller
{
    //
    public function index(){
        // Sem filtro
        //$cardapios = Cardapio::all();
        // Apenas cardapios dos restaurantes aos quais estao associados ao usuario
        // Aqui estou trazendo ids dos bares cadastrados e filtro no where in
        $bares = Auth::user()->bares()->select('id')->get()->toArray();
        $cardapios = Cardapio::whereIn('bar_id', $bares)->get();
        return view('admin.cardapios.index', compact('cardapios'));
    }
    
    public function novo(){
        
        
        $bares = Auth::user()->bares;

        return view('admin.cardapios.store', compact('bares'));
    }
    public function store(CardapioRequest $request){
        $cardapioData = $request->all();
        
        $validator = $request->validated();
        
        $bar = Bar::find($cardapioData['bar_id']);
        
        // Aqui crio o registro a partir do bar ao qual o cardapio pertence
        $bar->cardapios()->create($cardapioData);
        
        //Usando direto o assignment
        //$cardapio = new Cardapio();
        //$cardapio->create($cardapioData);
        
        flash('Cardapio criado com sucesso!')->success();
        return redirect()->route('cardapio.home');
    }
    public function edit(Cardapio $cardapio){ // aqui automaticamente pelo id ele ja traz o objeto
        
//        $bares = Bar::all(['id', 'nome']);
        // Trazendo apenas dos bares que pertencem ao usuario
          $bares = Auth::user()->bares;

        
        return view('admin.cardapios.edit', compact('cardapio', 'bares'));
        
    }
    
    public function update(CardapioRequest $request, $id){
        $cardapioData = $request->all();
        
        $validator = $request->validated();
        
        $cardapio = Cardapio::findOrFail($id); // aqui eu busco as informacoes do objeto pelo id, no caso o que identica e o mesmo nome da rota
        // A parte abaixo e "opcional" porque declarei no assignment do Model, sem esta linha tb funcionaria
        $cardapio->bar()->associate($cardapioData['bar_id']);
        $cardapio->update($cardapioData);
        
        
        flash('Cardapio atualizado com sucesso')->success();
        return redirect()->route('cardapio.edit', ['cardapio' => $id]);
    }
    
    public function delete($id){
        
        
        $cardapio = Cardapio::findOrFail($id); // aqui eu busco as informacoes do objeto pelo id, no caso o que identica e o mesmo nome da rota
        $cardapio->delete();
        
        
        flash('Cardapio removido com sucesso')->success();
    return redirect()->route('cardapio.home');
    }
    
    
}
