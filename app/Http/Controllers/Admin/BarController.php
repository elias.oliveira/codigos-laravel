<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\BarRequest;
use App\Bar;
use Auth;

class BarController extends Controller
{
    //
    public function index(){
        //$bares = Bar::where('owner_id', Auth::user()->id)->get();
        $bares = Auth::user()->bares;

        return view('admin.bares.index', compact('bares'));
    }
    
    public function novo(){
        return view('admin.bares.store');
    }
    public function store(BarRequest $request){
        $barData = $request->all();
        
        $validator = $request->validated();
        
        $user = Auth::user();
        $user->bares()->create($barData);
        
        //$bar = new Bar();
        ///$bar->create($barData);
        
        flash('Bar criado com sucesso!')->success();
        return redirect()->route('bar.home');
    }
    public function edit(Bar $bar){ // aqui automaticamente pelo id ele ja traz o objeto
        
        return view('admin.bares.edit', compact('bar'));
        
    }
    
    public function update(BarRequest $request, $id){
        $barData = $request->all();
        
        $validator = $request->validated();
        
        $bar = Bar::findOrFail($id); // aqui eu busco as informacoes do objeto pelo id, no caso o que identica e o mesmo nome da rota
        
        $bar->update($barData);
        
        flash('Bar atualizado com sucesso')->success();
        return redirect()->route('bar.edit', ['bar' => $id]);
    }
    
    public function delete($id){
        
        
        $bar = Bar::findOrFail($id); // aqui eu busco as informacoes do objeto pelo id, no caso o que identica e o mesmo nome da rota
        $bar->delete();
        
        
        flash('Bar removido com sucesso')->success();
        return redirect()->route('bar.home');
    }
    
    
}
