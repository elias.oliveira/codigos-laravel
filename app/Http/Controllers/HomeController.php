<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bar;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $bares = Bar::paginate(10);
        return view('home', compact('bares'));
    }
    
    public function get(Bar $slug)
    {
        // usei por causa do getRouteKeyName no bar, que posso trocar pelo id para encontrar
        $bar =  $slug;
        //$bar = Bar::whereSlug($slug)->first();
        // mesma coisa: $bar = Bar::where('slug', $slug)->first();
        return view('single', compact('bar'));
    }
}
