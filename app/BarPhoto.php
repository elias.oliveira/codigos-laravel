<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarPhoto extends Model
{
    //
    protected $table = 'bar_photos';
    protected $fillable = [
        'photo'
    ];
    public function bar()
    {
        //pertence ha
        return $this->belongsTo(Bar::class);
    }
}
