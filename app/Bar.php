<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
use App\User;


class Bar extends Model
{
    //
    
    use HasSlug;
    protected $table = 'bar';
    protected $fillable = [
        'nome', 'endereco', 'descricao', 'slug'
    ];
    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('nome')
            ->saveSlugsTo('slug');
    }
    
    // Para usar o parametro instanciando na chamada do metodo:
    public function getRouteKeyName()
    {
        return 'slug';
    }
    
    
    public function cardapios()
    {
        // Um restaurante possui varios cardapios(itens cardapio)
        return $this->hasMany(Cardapio::class);
    }
    
    public function photos()
    {
        
        return $this->hasMany(BarPhoto::class, 'bar_id');
    }
    
    public function dono()
    {
        return $this->belongsTo(User::class);
    }
    
}
