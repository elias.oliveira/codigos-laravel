<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cardapio extends Model
{
    //
   protected $table = 'cardapio'; 
   protected $fillable = [
        'nome', 'preco', 'bar_id'
    ];
   
    public function bar()
    {
        // o cardapio pertence a um restaurante
        return $this->belongsTo(Bar::class);
    }
}
