@extends('layouts.app')

@section('content')
<div class="container">
    <h1> Bares</h1>   
    <div class="row">
        @foreach($bares as $b)
            <div class="col-4">
                @if($b->photos()->count())
                <img src="{{asset('images/' . $b->photos()->first()->photo)}}" class="img-fluid">
                @endif
                <h2>
                    <a href="{{route('home.single', ['slug' => $b->slug])}}">{{$b->nome}}</a>
                </h2>
                <p>{{str_limit($b->descricao, 70) }}</p>
            </div>
        @endforeach
        
    </div>
    {{$bares->links()}}
</div>
@endsection
