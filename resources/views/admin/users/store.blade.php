@extends('layouts.app')

@section('content')
<div class="container">
    <h1> Inserir Usuário </h1> 
    <hr>
        <form  action="{{route('user.store')}}" method="post">

            {{csrf_field()}} 
             <div class="form-group">
               
                <label>Nome do Usuário</label>
                <input type="text" class="form-control @if($errors->has('name')) is-invalid @endif" name="name" value="{{old('name')}}">
                @if($errors->has('name'))
                    <span class="invalid-feedback">
                       @foreach($errors->get('name') as $n)
                       {{$n}}
                       @endforeach
                    </span>
                @endif

                <label>Email</label>
                <input type="email" class="form-control @if($errors->has('email')) is-invalid @endif" name="email" value="{{old('email')}}">
                 @if($errors->has('email'))
                    <span class="invalid-feedback">
                        {{$errors->first('email')}}
                    </span>    
                @endif
                
                <label>Senha</label>
                <input type="password" class="form-control @if($errors->has('password')) is-invalid @endif" name="password" value="{{old('password')}}">
                 @if($errors->has('password'))
                    <span class="invalid-feedback">
                        {{$errors->first('password')}}
                    </span>    
                @endif
                
                <label>Comfirmar Senha</label>
                <input type="password" class="form-control @if($errors->has('password_confirmation')) is-invalid @endif" name="password_confirmation" value="{{old('password_confirmation')}}">
                 @if($errors->has('password_confirmation'))
                    <span class="invalid-feedback">
                        {{$errors->first('password_confirmation')}}
                    </span>    
                @endif

                <Br>
                <input type="submit" class="btn btn-primary" value="Cadastrar"> 
                <a href="{{route('user.home')}}"   class="btn btn-warning" >  VOLTAR  </a> 
            </div>
        </form>
</div>
@endsection()