@extends('layouts.app')

@section('content')
<div class="container">
    <h2> Edicao cardapios</h2> 
    <hr>
    <form action="{{route('cardapio.update', ['id' => $cardapio->id ])}}" method="post">
        
        <div class="form-group">
        {{csrf_field()}} 

            <label>Nome do Cardapio</label>
            <input class="form-control @if($errors->has('nome')) is-invalid @endif" type="text" name="nome" value="{{$cardapio->nome}}">
             @if($errors->has('nome'))
                <span class="invalid-feedback">
                    @foreach($errors->get('nome') as $n)
                    {{$n}}
                    @endforeach
                </span>
            @endif

            <label>Preço</label>
            <input type="text" class="form-control @if($errors->has('preco')) is-invalid @endif"  name="preco" value="{{$cardapio->preco}}">
              @if($errors->has('preco'))
                <span class="invalid-feedback">
                    {{$errors->first('preco')}}
                </span>
              @endif
            
             <label>Bar</label>
                
                 <select class="form-control" name="bar_id">
                     @foreach( $bares as $b)
                        <option  
                            @if($b->id == $cardapio->bar_id) selected=selected @endif
                            value="{{$b->id}}" >
                                {{$b->nome}}
                        </option>
                     @endforeach
                 </select>
             
             @if($errors->has('bar_id'))
                <span class="">
                    {{$errors->first('bar_id')}}   
                </span>
             @endif
                  
           
            <input class="btn btn-primary" type="submit" value="Atualizar"> 
            <a href="{{route('cardapio.home')}}"   class="btn btn-warning" >  VOLTAR  </a> 

        </div>
    </form>
</div>
@endsection()
