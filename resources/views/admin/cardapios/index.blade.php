@extends('layouts.app')

@section('content')
<div class="container">
    <h1 class="float-left"> Cardapios </h1> 

    <a href="{{route('cardapio.new')}}"   class="float-right btn btn-success" >  NOVO  </a> 
    

    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Nome</th>
                <th>Bar</th>
                <th>Criado em</th>
                <th>Acões</th>
            </tr>
        </thead>
        <tbody>
            @foreach($cardapios as $b)
                <tr>
                    <td>{{$b->id}}</td>
                    <td>{{$b->nome}}</td>
                    <td>
                        <a href="{{route('bar.edit', ['bar' => $b->bar->id])}}">
                            {{$b->bar->nome}}
                        </a>
                    </td>
                    <td>{{$b->created_at}}</td>
                    <td>
                        <a href="{{route('cardapio.edit', ['cardapio' => $b->id])}}" class="btn btn-primary">EDITAR </a>
                        <a href="{{route('cardapio.remove', ['id' => $b->id])}}" class="btn btn-danger" >EXCLUIR </a>
                    </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection()