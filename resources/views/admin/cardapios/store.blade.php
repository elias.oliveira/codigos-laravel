@extends('layouts.app')

@section('content')
<div class="container">
    <h1> Insercao cardapios</h1> 
    <hr>
        <form  action="{{route('cardapio.store')}}" method="post">

            {{csrf_field()}} 
             <div class="form-group">
               
                <label>Nome do Cardapio</label>
                <input type="text" class="form-control @if($errors->has('nome')) is-invalid @endif" name="nome" value="{{old('nome')}}">
                @if($errors->has('nome'))
                    <span class="invalid-feedback">
                       @foreach($errors->get('nome') as $n)
                       {{$n}}
                       @endforeach
                    </span>
                @endif

                <label>Preço</label>
                <input type="text" class="form-control @if($errors->has('preco')) is-invalid @endif" name="preco" value="{{old('preco')}}">
                 @if($errors->has('preco'))
                    <span class="invalid-feedback">
                        {{$errors->first('preco')}}
                    </span>    
                @endif
                
                
                 <label>Bar</label>
                
                 <select class="form-control" name="bar_id">
                     <option value="" > Selecione um Bar para este item do Cardápio</option> 
                     @foreach( $bares as $b)
                        <option value="{{$b->id}}"
                                @if($b->id == old('bar_id')) selected=selected @endif
                                >{{$b->nome}}</option>
                     @endforeach
                 </select>
                 
                  @if($errors->has('bar_id'))
                  <span class="" style="color:red;">
                        {{$errors->first('bar_id')}}
                    </span>    
                  @endif
                
                <Br>
                <input type="submit" class="btn btn-primary" value="Cadastrar"> 
                <a href="{{route('cardapio.home')}}"   class="btn btn-warning" >  VOLTAR  </a> 
            </div>
        </form>
</div>
@endsection()