@extends('layouts.app')

@section('content')
<div class="container">
    <h1> Insercao bares</h1> 
    <hr>
        <form  action="{{route('bar.store')}}" method="post">

            {{csrf_field()}} 
             <div class="form-group">
               
                <label>Nome do Bar</label>
                <input type="text" class="form-control @if($errors->has('nome')) is-invalid @endif" name="nome" value="{{old('nome')}}">
                @if($errors->has('nome'))
                    <span class="invalid-feedback">
                       @foreach($errors->get('nome') as $n)
                       {{$n}}
                       @endforeach
                    </span>
                @endif

                <label>Endereço</label>
                <input type="text" class="form-control @if($errors->has('endereco')) is-invalid @endif" name="endereco" value="{{old('endereco')}}">
                 @if($errors->has('endereco'))
                    <span class="invalid-feedback">
                        {{$errors->first('endereco')}}
                    </span>    
                @endif

                <label>Fale sobre o Bar</label>
                <textarea class="form-control form-control @if($errors->has('descricao')) is-invalid @endif" name="descricao"   id="" cols="30" rows="10">{{old('descricao')}}</textarea>
                 @if($errors->has('descricao'))
                    <span class="invalid-feedback">
                        {{$errors->first('descricao')}}
                    </span>
                @endif

                <Br>
                <input type="submit" class="btn btn-primary" value="Cadastrar"> 
                <a href="{{route('bar.home')}}"   class="btn btn-warning" >  VOLTAR  </a> 
            </div>
        </form>
</div>
@endsection()