@extends('layouts.app')

@section('content')
<div class="container">
    <h2> Edicao bares</h2> 
    <hr>
    <form action="{{route('bar.update', ['id' => $bar->id ])}}" method="post">
        
        <div class="form-group">
        {{csrf_field()}} 

            <label>Nome do Bar</label>
            <input class="form-control @if($errors->has('nome')) is-invalid @endif" type="text" name="nome" value="{{$bar->nome}}">
             @if($errors->has('nome'))
                <span class="invalid-feedback">
                    @foreach($errors->get('nome') as $n)
                    {{$n}}
                    @endforeach
                </span>
            @endif

            <label>Endereço</label>
            <input type="text" class="form-control @if($errors->has('endereco')) is-invalid @endif"  name="endereco" value="{{$bar->endereco}}">
              @if($errors->has('endereco'))
                <span class="invalid-feedback">
                    {{$errors->first('endereco')}}
                </span>
            @endif

            <label>Fale sobre o Bar</label>
            <textarea class="form-control @if($errors->has('descricao')) is-invalid @endif"  name="descricao" id="" cols="30" rows="10" >{{$bar->descricao}}</textarea>
             @if($errors->has('descricao'))
                <span class="invalid-feedback">
                    {{$errors->first('descricao')}}
                </span>
            @endif
            <br>
            <input class="btn btn-primary" type="submit" value="Atualizar"> 
            <a href="{{route('bar.home')}}"   class="btn btn-warning" >  VOLTAR  </a> 

        </div>
    </form>
</div>
@endsection()
