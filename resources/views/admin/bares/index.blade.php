@extends('layouts.app')

@section('content')
<div class="container">
    <h1 class="float-left"> Bares </h1> 

    <a href="{{route('bar.new')}}"   class="float-right btn btn-success" >  NOVO  </a> 
    

    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Nome</th>
                <th>Criado em</th>
                <th>Acões</th>
            </tr>
        </thead>
        <tbody>
            @foreach($bares as $b)
                <tr>
                    <td>{{$b->id}}</td>
                    <td>{{$b->nome}}</td>
                    <td>{{$b->created_at}}</td>
                    <td>
                        <a href="{{route('bar.edit', ['bar' => $b->id])}}" class="btn btn-primary">EDITAR </a>
                        <a href="{{route('bar.photo', ['id' => $b->id])}}" class="btn btn-warning" >FOTOS </a>
                        <a href="{{route('bar.remove', ['id' => $b->id])}}" class="btn btn-danger" >EXCLUIR </a>
                    </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection()