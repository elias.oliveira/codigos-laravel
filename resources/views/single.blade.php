@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12" >
            <h2 >
                {{$bar->nome}}
            </h2> 
            <p>
                {{$bar->descricao}}
            </p>
            <p>
            <address> Endereço: {{$bar->endereco}}</address>
            </p>
            <hr>
        </div>  
        <div class="col-12">
            Cardápio:
            <ul class="list-group">
                @foreach($bar->cardapios as $c)
                <li class="list-group-item">{{$c->nome}}<br>
                    {{number_format($c->preco, '2', ',', '.')}}
                    
                </li>
                @endforeach
            </ul>
        </div>
       </div>  
    <Br>
        <h2>Fotos</h2>
            <hr>
        <div class="row">
            @if($bar->photos()->count())
                @foreach($bar->photos as $photo)
                <div class="col-4">
                    <img src="{{asset('images/' . $photo->photo)}}" class="img-fluid">
                </div>    
                @endforeach
            @else
            <span class="alert alert-warning"> Sem fotos para este Bar </span>
            @endif
        </div>
   
    
</div>
@endsection
